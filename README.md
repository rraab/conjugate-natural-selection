# Conjugate Natural Selection

This code accompanies a paper by the same name submitted hosted on Arxiv.

# Quick-start

## Installation

* `ffmpeg` must be installed for video output

`requirements.txt` is included for use with `pip`

## Running

```
python cns/main.py
python cns/wiener.py
```

# Contributing 

This repository is set up for tight version-control management with
[pyenv](https://github.com/pyenv/pyenv), 
[poetry](https://python-poetry.org/), and
[pre-commit](https://pre-commit.com/)

## Development Environment

Use the right python version and install the dependencies
```
pyenv local 3.11.0
poetry env use 3.11.0
poetry install
```

## Environment Activation

The environment can be activated / deactivated with:
* `poetry shell` /  `exit` within the shell
* [direnv](https://direnv.net/)

I use the last option. First, ensure it's setup up correctly 
[for poetry](https://github.com/direnv/direnv/wiki/Python/#poetry), 
then  write an `.envrc` file in the repository root that reads

```
layout poetry
which python
```

followed once by the command to allow automatic activation when the current 
working directory is under the repository root:
```
direnv allow PATH_TO_REPO_ROOT
```
